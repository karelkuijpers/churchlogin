<?php

defined('TYPO3') or die('Access denied.');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('be_users', [
	"tx_churchadmin_fe_user_id" => [		
		"exclude" => 0,		
		"label" => "LLL:EXT:churchadmin/locallang_db.xml:be_users.tx_churchadmin_fe_user_id",		
		"config" => [
			"type" => "group",	
			"internal_type" => "db",	
			"allowed" => "fe_users",	
			"size" => 1,	
			"minitems" => 0,
			"maxitems" => 1,
			]
		]
	]
);
	
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'be_users',
    'tx_churchadmin_fe_user_id'
);

