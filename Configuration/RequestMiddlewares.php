<?php
use Parousia\Churchlogin\Middleware\FrontendUserPostLookup;

return [
    'frontend' => [
        'churchlogin-postfeuserlookup' => [
            'target' => FrontendUserPostLookup::class,
            'after' => [
                'typo3/cms-frontend/authentication',
            ],
        ],
    ],
];