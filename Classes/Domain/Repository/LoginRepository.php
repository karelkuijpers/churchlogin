<?php

namespace Parousia\Churchlogin\Domain\Repository;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Configuration\ConfigurationManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Database\Query\QueryHelper;


/**
 * Class LoginRepository
 *
 * @package Parousia\Churchlogin\Domain\Repository
 *
 * return TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class LoginRepository extends Repository
{

 /**
     * Function findPersondata 
     *
     * @param int  $id_person                 id of persoon
     *
	 * @return array
     */
	public function findPersondata($id_person)
	{
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('persoon');
		$fields='Select upper(concat_ws(" ",achternaam,voornamen,roepnaam'.
		',AES_DECRYPT(emailadres,@password)'.
		',AES_DECRYPT(mobieltelnr,@password)'.
		',beroep, date_format(geboortedatum,"%d%m%Y"),geboorteplaats,date_format(trouwdatum,"%d%m%Y")'.
		',straatnaam,replace(postcode," ",""),woonplaats'.
		',AES_DECRYPT(telefoonnr_vast,@password))) as personstring,id_vader,id_moeder,id_partner'.
		' FROM persoon as tp, adres as ta where ta.uid=AES_DECRYPT(tp.id_adres,@password) and ';
		$statement=$fields.'tp.uid= '.$id_person; 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findPersondata statement:: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		$rows = $connection->executeQuery($statement)->fetchAll();
		$row=$rows[0];
		
		//findRelatives
		$statement=$fields.
		' (tp.id_partner="'.$id_person.'" or tp.id_vader="'.$id_person.'" or tp.id_moeder="'.$id_person.'" or tp.uid="'.$row['id_vader'].'" or tp.uid="'.$row['id_moeder'].'")';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findRelatives statement:: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		
		$rows = $connection->executeQuery($statement)->fetchAll();
		$rows[]=$row;
		return $rows;

    }
	
	
 /**
     * Function findRegisterdata 
     *
     * @param int  $id_person                 id of persoon
     *
	 * @return array
     */
	public function findRegisterdata($geboortedatum,$voornaam,$achternaam)
	{
		//	Samenstellen van zoekargument
		$zoekargument='';
		if ($voornaam<>"")
		{	$a_naam=explode(" ",$voornaam); // splits in deelnamen etc.
			foreach ($a_naam as $naam)
			{	$naam=trim($naam);
				$naam=addSlashes($naam);
				if (!empty($naam))
				{	if (!empty($zoekargument))$zoekargument.=" AND ";
					$zoekargument.="(voornamen like '%".$naam."%' or roepnaam like '%".$naam."%') ";
				}
			}
		}
		if ($achternaam<>"")
		{	
			$a_naam=explode(" ",$achternaam); // splits in deelnamen etc.
			foreach ($a_naam as $naam)
			{	$naam=trim($naam);
				$naam=$naam=addSlashes($naam);;
				if (!empty($naam))
				{	if (!empty($zoekargument))$zoekargument.=' AND ';
					$zoekargument.="(achternaam like '%".$naam."%' or tussenvoegsel like '%".$naam."%')";
				}
			}
		}
		if (!empty($zoekargument))$zoekargument.=' AND ';
		$zoekargument.="(geboortedatum is null or geboortedatum like '%".$geboortedatum."%' or geboortedatum='0000-00-00')";
		
		$zoekargument.=" AND deleted=0 ";

		$statement="SELECT replace(concat_ws(' ',roepnaam,tussenvoegsel,achternaam),'  ',' ') as naam,geboortedatum,uid,AES_DECRYPT(emailadres,@password) as emailadres, bezoeker".
		' FROM persoon where '.$zoekargument.' order by geboortedatum desc';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'UserRepository findRegisterdata statement:'.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('persoon');
		$result = $connection->executeQuery('SELECT @password:="'.$GLOBALS['TYPO3_CONF_VARS']['PARCHURCH']['Connections']['parsl'].'"');
		$rows = $connection->executeQuery($statement)->fetchAll();
		try {
		      return $rows;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 

    }
 /**
     * Function checkAccountUsed 
     *
     * @param int  $id_person                 id of persoon
     * $param string $accountname
	 * @return found number
     */
	public function checkAccountUsed($id_persoon,$accountname)
	{
		// check if accountname allready exists for other person:
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
		$foundnumber = $queryBuilder
	   		->count('uid')
	   		->from('fe_users')
	   		->where(
	      		$queryBuilder->expr()->neq('person_id', $queryBuilder->createNamedParameter($id_persoon, \PDO::PARAM_INT)),
	      		$queryBuilder->expr()->eq('username', $queryBuilder->createNamedParameter($accountname, \PDO::PARAM_STR))
	    	)
	   		->execute()
	   		->fetchOne(0);
		return $foundnumber;
	}
	
 /**
     * Function recordAccount 
     *
     * @param int  $id_person                 id of persoon
     * $param string $accountname
	 * returns array with g_uid and password // g_uid=uid of account
     */
	public function recordAccount($id_persoon,$accountname,$spid,$temppwduration)
	{
		$error='';
		$g_uid='';
		$ww='';
		$geblokkeerd=false;
		
		// get general usergroup for each loggedin user
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchauthreg');					
		$defaultusergroup = $extensionConfiguration['defaultusergroup'];	
		// find default usergroup:
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'recordAccount insert gebruiker in pid: '.$spid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		$row = GeneralUtility::makeInstance(ConnectionPool::class)
		    ->getConnectionForTable('fe_groups')
	    	->select(
	       		['uid'], // fields to select
		       	'fe_groups', // from
	        	[ 'title' => $defaultusergroup ] // where
    		)
    		->fetchAssociative();
		if (is_array($row)) $gg_uid=$row['uid'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'recordAccount insert/update gebruiker with usergroup: '.$defaultusergroup.'; gg_uid:'.$gg_uid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');

		// Determine temporary password
		//$ww=strtolower(date("D").date("Hi"));
		//$ww=date("D").substr(md5(uniqid(rand(), true)),0,6);
		$ww=$this->generatePassword();
		$geblokkeerd=0;
		// check of al bestaande account:
		$row = GeneralUtility::makeInstance(ConnectionPool::class)
		    ->getConnectionForTable('fe_users')
    		->select(
        		['uid', 'huidiginloglevel','geblokkeerd','lastUpdPwd','TimeTempPassword'], // fields to select
        		'fe_users', // from
        		[ 'person_id' => (int)$id_persoon ] // where
    		)
    		->fetchAssociative();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'recordAccount user row : '.urldecode(http_build_query($row,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
									
		if (is_array($row) && count($row)>0)
		{
			if (empty($row['lastUpdPwd']) && !empty($row['TimeTempPassword']))
			{
				$DateTempPassw=new \DateTime($row["TimeTempPassword"]);
				$interval=new \DateInterval('PT'.$temppwduration.'M');
				$now= new \DateTime("");
				$verschil=$now->getTimestamp() - $DateTempPassw->getTimestamp();
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'recordAccount now: '.$now->format("Y-m-d H:i:s").'; TimeTempPassword: '.$DateTempPassw->format("Y-m-d H:i:s").'; verschil: '.$verschil."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
				if ($verschil<5)
				{
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'recordAccount 2x geclickt '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
					$error='account.ignore';
				}
				elseif ($verschil < $temppwduration*60)
				{
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'recordAccount email al verzonden'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
					$error='account.accpwdsend';
				}
			}
			if (empty($error))
			{
				$g_uid = $row['uid'];
				$result = GeneralUtility::makeInstance(ConnectionPool::class)
			    	->getConnectionForTable('fe_users')
	   				->update(
						'fe_users',
	       				array('username'=>$accountname,'password'=>SHA1($ww),'lastUpdPwd'=>'','geblokkeerd'=>0,'LoginErrorCount'=>0,'TimeTempPassword'=>(new \DateTime())->format("Y-m-d H:i:s")), // fields to update
	       				[ 'person_id' => (int)$id_persoon ] // where
	    		);
				if ($result==1) 
				{
					$this->securityupdate(NULL,$id_persoon);
					$geblokkeerd=$row['geblokkeerd'];
				}
			}
		}
		else 
		{	
			// voeg gebruiker toe:
			$connectionfortable = GeneralUtility::makeInstance(ConnectionPool::class)
		    	->getConnectionForTable('fe_users');			
    		$connectionfortable->insert(
					'fe_users',
					array('username'=>$accountname,'password'=>SHA1($ww),'person_id'=>$id_persoon,'usergroup'=>$gg_uid,'securitylevel'=>'licht',
					'usergroup'=>$gg_uid,'huidiginloglevel'=>'licht','pid'=>intval($spid)));
			// bepaal g_uid:
			$g_uid = $connectionfortable->lastInsertId('fe_users');
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'recordAccount inserted gebruiker: '.$g_uid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');

		}
		return ['g_uid'=>$g_uid,'password'=>$ww,'geblokkeerd'=>$geblokkeerd,'key' =>$error];
	}
	
	function generatePassword($length=15)
	{
		 $pass = '';
//		 $parts = array_merge(range(0, 9),range('a', 'z'),range('A', 'Z'),str_split('!@#$%&*?'));
		 $parts = array_merge(range(0, 9),range('a', 'z'),range('A', 'Z'));
		 $lnparts=count($parts)-1;
		 for ($i=0; strlen($pass) <= $length; $i++) {
		 	$ptr=random_int(0,$lnparts);		  
		  	$pass .= $parts[$ptr];
		 }
		 $pass=str_shuffle($pass);
		 return $pass;
	}
	/**
	   * recordLogin function 
	   *
	   * param int  $uid                 id of record
	   *
	   */
	public function recordLogin($uid,$AantalMaalIngelogd)
	{
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
		$queryBuilder->getRestrictions()->removeAll();
		$queryBuilder->update('fe_users')
			->set('AantalMaalIngelogd', $AantalMaalIngelogd+1)
			->set('LoginErrorCount', 0)
			->set('tstamp', (int) (new \DateTime())->getTimestamp())
			->where(
				$queryBuilder->expr()->eq(
					'uid',
					$queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
				)
			)
			->execute();
	}
	
	/**
	   * recordLoginError function 
	   *
	   * param int  $uid                 id of record
	   * param int  $LoginErrorCount     login errors count
	   *
	   */
	public function recordLoginError($uid,$LoginErrorCount)
	{
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
		$queryBuilder->getRestrictions()->removeAll();
		$queryBuilder->update('fe_users')
			->set('LoginErrorCount', $LoginErrorCount)
			->set('tstamp', (int) (new \DateTime())->getTimestamp())
			->where(
				$queryBuilder->expr()->eq(
					'uid',
					$queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
				)
			)
			->execute();
	}
	
	/**
	   * record2FAEnabled function 
	   *
	   * param int  $uid                 id of record
	   * param bool  $TwoFAenabled     
	   *
	   */
	public function record2FAEnabled( int $uid,bool $TwoFAenabled)
	{
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
		$queryBuilder->getRestrictions()->removeAll();
		$queryBuilder->update('fe_users')
			->set('tx_googleauth_enabled', $TwoFAenabled)
			->set('tstamp', (int) (new \DateTime())->getTimestamp())
			->where(
				$queryBuilder->expr()->eq(
					'uid',
					$queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
				)
			)
			->execute();
	}
	/**
	   * recorBirthdate function 
	   *
	   * param int  $uid                 id of record
	   * param string $birthdate
	   *
	   */
	public function recordBirthdate($uid,$birthdate)
	{
		// vastleggen geboortedatum:
	    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('persoon');
	    //$queryBuilder->getRestrictions()->removeAll();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'recordBirthdate date:'.$birthdate.' for user :'.$uid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
	    $queryBuilder->update('persoon')
	         ->set('geboortedatum', $birthdate)
	         ->where(
	              $queryBuilder->expr()->eq(
	                    'uid',
	                    $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
	                    )
	              )
	         ->execute();
	}
	
	/**
	* function securityupdate
	*/
	public function securityupdate($myid = NULL,$personid = NULL,$updatecurr = false,$fe_user = NULL){
	// Bijwerken securitylevel van gebruikerswachtwoorden n.a.v. wijzigingen in autorisaties
	// connectie met database moet bestaan
	
		// Doorloop alle useraccounts en 
		// bepaal maximale zwaarte van securitylevel van bijbehorende functionaliteiten:

		$sqlst_ =	"SELECT max(f.securitylevel) as funclevel, g.uid as userid,g.username as naam,g.securitylevel as acclevel,g.huidiginloglevel, g.person_id as person, group_concat(distinct t.omschrijving separator ',') as bediening, group_concat(distinct b.uid separator ',') as beuser ".
			"FROM rm_functionaliteit f, rm_gg_f as gg_f,  fe_users g ".
			" LEFT OUTER JOIN bediening as t ON (g.person_id=t.id_bedieningsleider or g.person_id IN (t.secretariaat))".
			" LEFT OUTER JOIN be_users as b ON (g.uid=b.tx_churchadmin_fe_user_id and b.deleted=0) ".
			" WHERE ".
			"f.f_uid=gg_f.f_uid AND ".
			"FIND_IN_SET(gg_f.gg_uid,g.usergroup) ";
		if (!empty($myid)) $sqlst_ .=" AND g.uid=".$myid; 
		elseif(!empty($personid)) $sqlst_ .=" AND g.person_id=".$personid;
		//elseif(!empty($f_uid))$sqlst_ .=" AND f.f_uid=".$f_uid; 
		$sqlst_ .=	" group by g.uid";
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'securityupdate sql: '.$sqlst_."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');

		$rows = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('persoon')->executeQuery($sqlst_)->fetchAll();
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'securityupdate rows: '.GeneralUtility::array2xml($rows)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');

		foreach ($rows as $row)
		{	$max=$row["funclevel"];
			if (!is_null($row["bediening"]) and $max=="licht") $max="middel";
			if (!is_null($row["beuser"]) and $max=="licht") $max="middel";
			$fields=[ 'securitylevel' => $max];
			$currentlevel=$row['huidiginloglevel'];
			if ($updatecurr)
			{
				if ($currentlevel<>$max)$currentlevel=$max;
				$fields['huidiginloglevel'] = $currentlevel;
			}
			if ($max<>$row["acclevel"] or $currentlevel<>$row['huidiginloglevel'])
			{	// pas level van wachtwoord aan:
				$result = GeneralUtility::makeInstance(ConnectionPool::class)
		    		->getConnectionForTable('fe_users')
    				->update(
						'fe_users',
        				$fields, // fields to update
        				[ 'uid' => $row["userid" ]] // where
	    			);
				if ($row['person']==$personid and isset($fe_user->user))
				{	$fe_user->user['securitylevel']=$max;
					$fe_user->user['huidiginloglevel']=$currentlevel;
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'securityupdate :'.$max.' for user :'.$personid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
				}
			}
		}
	}

    /**
     * Get a user from DB by username
     * provided for usage from services
     *
     * @param array $dbUser User db table definition: $this->db_user
     * @param string $username user name
     * @param string $extraWhere Additional WHERE clause: " AND ...
     * @return mixed User array or FALSE
     */
    public function fetchUserRec($dbUser, $username, $extraWhere = '')
    {
        trigger_error('This method will be removed in TYPO3 v10.0.', E_USER_DEPRECATED);
        $user = false;
        if ($username || $extraWhere) {
            $query = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($dbUser['table']);
            $query->getRestrictions()->removeAll()
                ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

            $constraints = array_filter([
//                QueryHelper::stripLogicalOperatorPrefix($dbUser['check_pid_clause']),
//                QueryHelper::stripLogicalOperatorPrefix($dbUser['enable_clause']),
                QueryHelper::stripLogicalOperatorPrefix($extraWhere),
            ]);

            if (!empty($username)) {
                array_unshift(
                    $constraints,
                    $query->expr()->eq(
                        $dbUser['username_column'],
                        $query->createNamedParameter($username, \PDO::PARAM_STR)
                    )
                );
            }

            $user = $query->select('*')
                ->from($dbUser['table'])
                ->where(...$constraints)
                ->execute()
                ->fetch();
        }
        return $user;
    }

	/**
	* function blockuser
	*/
	/**
	* function blockuser
	*/
	public function blockuser($uid)
	{
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
		$queryBuilder->getRestrictions()->removeAll();
		$queryBuilder->update('fe_users')
			->set('geblokkeerd', '1')
			->set('tstamp', (int) (new \DateTime())->getTimestamp())
			->where(
				$queryBuilder->expr()->eq(
					'uid',
					$queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
				)
			)
			->execute();
	}
	
	/**
	*
	* UpdateUserTable
	*/
	public function UpdateUserTable($WachtwoordNw,$aPrvww,$huidiglevel,$uid)
	{
	    $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
	    $queryBuilder->getRestrictions()->removeAll();
	    $queryBuilder->update('fe_users')
	         ->set('password', $WachtwoordNw)
	         ->set('lastUpdPwd', date('YmdHis'))
	         ->set('pswPrevious1', implode(",",$aPrvww))
	         ->set('huidiginloglevel', $huidiglevel)
			 ->set('LoginErrorCount', 0)
	         ->set('tstamp', (int) (new \DateTime())->getTimestamp())
	         ->where(
	              $queryBuilder->expr()->eq(
	                    'uid',
	                    $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
	                    )
	              )
			 ->execute();

	}

}

