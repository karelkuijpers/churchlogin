<?php
namespace Parousia\Churchlogin\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***
 *
 * This file is part of the "Churchperesreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * Account is a request to become part of the church
 */
class Account extends AbstractEntity
{
     /**
     * voornaam
     *
     * @var string
     */
    protected $voornaam = '';

  /**
     * achternaam
     *
     * @var string
     */
    protected $achternaam = '';

    /**
     * geboortedatum
     *
     * @var string
     */
    protected $geboortedatum = NULL;

    /**
     * username
     *
     * @var string
     */

    protected $username = '';

    /**
     * username
     *
     * @var string
     */


    protected $voorwaarde = false;

    /**
     * voorwaarde
     *
     * @var bool
     */

/**
* Getters and Setters
*/

    /**
     * Returns the voornaam
     *
     * @return string $voornaam
     */
    public function getVoornaam(): string
    {
        return $this->voornaam;
    }
    /**
     * Sets the voornaam
     *
     * @param string $voornaam
     */
    public function setVoornaam($voornaam): void
    {
        $this->voornaam = $voornaam;
    }

   /**
     * Returns the achternaam
     *
     * @return string $achternaam
     */
    public function getAchternaam(): string
    {
        return $this->achternaam;
    }

    /**
     * Sets the achternaam
     *
     * @param string $achternaam
     */
    public function setAchternaam($achternaam): void
    {
        $this->achternaam = $achternaam;
    }

   /**
     * Returns the geboortedatum
     *
     * @return string|null $geboortedatum
    */
    public function getGeboortedatum(): ?string
    {
        return $this->geboortedatum;
    }
	/**
     * Sets the geboortedatum
     *
     * @param \DateTime $geboortedatum
     * @return void
    */
    public function setGeboortedatum($geboortedatum): void
    {
        $this->geboortedatum = $geboortedatum;
    }

    /**
     * Returns the username
     *
     * @return string $username
     */
    public function getusername(): string
    {
        return $this->username;
    }

    /**
     * Sets the username
     *
     * @param string $username
    */
    public function setusername($username): void
    {
        $this->username = $username;
    }

    /**
     * Returns the voorwaarde
     *
     * @return bool $voorwaarde
     */
    public function getvoorwaarde(): bool
    {
        return $this->voorwaarde;
    }

    /**
     * Sets the voorwaarde
     *
     * @param bool $voorwaarde
     */
    public function setvoorwaarde($voorwaarde): void
    {
        $this->voorwaarde = $voorwaarde;
    }

    /**
     * Returns the deleted
     *
     * @return bool $deleted
     */
    public function getDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Sets the deleted
     *
     * @param bool $deleted
     */
    public function setDeleted($deleted): void
    {
        $this->deleted = $deleted;
    }
}