<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
namespace Parousia\Churchlogin\Controller;

use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Authentication\logintype;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Core\Security\RequestToken;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;

//use Parousia\Churchlogin\Controller\RSA;
use Parousia\Churchpersreg\Hooks\FluidEmailReal;
use Parousia\Churchlogin\Domain\Model\User;
use Parousia\Churchlogin\Domain\Repository\LoginRepository;
use Parousia\Churchlogin\Domain\Model\Account;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\RawMessage;

use Tx\Authenticatorfe\Auth\TokenAuthenticator;
use TYPO3\CMS\Core\Authentication\AbstractUserAuthentication;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;


/**
 * Used for plugin login
 */
class LoginController extends ActionController
{
	var $prefixId      = 'churchlogin';		// Same as class name
	var $scriptRelPath = 'pi1/class.churchlogin.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'churchlogin';	// The extension key.
	var $redirect_url="";
	var $seclevel="licht";
	var $wwreden="";  //1=eerste keer inloggen,2=verlopen,3=zwaardere functionaliteit
	var $username="";
	var $uid="";
	var $Wachtwoord="";
	var $NwPassword='';
	var $errcnt=0;
	var $blockedupto='';
	var $errcntUser=0;
	var $statelogin='';
	var $logintype='';
	var $extPath	='';
	var $lstupd;
	var	$achternaam;
	var	$voornaam;
	var	$geboortedatum;
	var	$accountname;
	var $spid;
	var $args;
	//var $context;
	var $refresh='0';
	var $defaultusergroup;
	var $passwordduration = 365; //days
	var $passwordupdatetime  = 60; //days
	var $temppwduration= 35; //minutes
	var $tempblockloginduration = 10; //minutes
	var $pagehome = '/homepage';
	var $pagemylogin = '';
	var $pagemy2FA = '';	
	var $error=array();
	var $passwordstrength=0;
	var $account=array();
	var $twoFA=0;
	var $googlepass='';
	var $frontendController;


    /**
     * @var AbstractUserAuthentication
     */
    protected $fe_user = null;
	
    /**
     * @var LoginRepository
     */
    protected $loginRepository;

    /**
     * @var UserAspect
     */
    protected $userAspect;


    public function __construct(
        LoginRepository $loginRepository
    ) {
        $this->loginRepository = $loginRepository;		
        $this->userAspect = GeneralUtility::makeInstance(Context::class)->getAspect('frontend.user');
    }

    /**
     * Initialize redirects
     */
    public function initializeAction(): void
    {
		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction args : '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$this->fe_user=$this->request->getAttribute('frontend.user');
		$this->frontendController = $this->request->getAttribute('frontend.controller');
		$this->conf['lang'] = $this->frontendController->config['config']['language'];
		$this->conf['lang']=$this->request->getAttribute('language');

//		if(is_array($this->request->getParsedBody()))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction getParsedBody : '.urldecode(http_build_query($this->request->getParsedBody(),NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//		if(is_array($this->request->getQueryParams()))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction getQueryParams : '.urldecode(http_build_query($this->request->getQueryParams(),NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');


        $this->logintype = (string)($this->request->getParsedBody()['logintype'] ?? $this->request->getQueryParams()['logintype'] ?? '');
        $this->wwreden = (string)($this->request->getParsedBody()['wwreden'] ?? $this->request->getQueryParams()['wwreden'] ?? '');
        $this->twoFA = (string)($this->request->getParsedBody()['twoFA'] ?? $this->request->getQueryParams()['twoFA'] ?? ''); 
        $this->googlepass = (string)($this->request->getParsedBody()['googlepass'] ?? $this->request->getQueryParams()['googlepass'] ?? ''); 
        $this->username = (string)($this->request->getParsedBody()['username'] ?? $this->request->getQueryParams()['username'] ?? '');
        $this->passwordstrength = (string)($this->request->getParsedBody()['percentage'] ?? $this->request->getQueryParams()['percentage'] ?? '');
        if (isset($this->args['error']))$this->error = $this->args['error'];
        if (isset($this->args['statelogin']))$this->statelogin = $this->args['statelogin'];
        if (isset($this->args['account']))$this->account = $this->args['account'];
        $this->uid = (string)($this->request->getParsedBody()['uid'] ?? $this->request->getQueryParams()['uid'] ?? '');
        $this->NwPassword = (string)($this->request->getParsedBody()['NwPassword'] ?? $this->request->getQueryParams()['NwPassword'] ?? '');

//		if (array_key_exists('statelogin',$this->args))$this->logintype = (string)$this->args['statelogin'];

//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction logintype : '.$this->logintype.'; user logged in:'.$this->userAspect->isLoggedIn()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchlogin');	
		$this->spid=$extensionConfiguration['storagePid'];
		$this->passwordduration = (int)$extensionConfiguration['passwordduration'] * 86400;
		$this->passwordupdatetime  = (int)$extensionConfiguration['passwordupdatetime'] * 86400;
		$this->temppwduration  = (int)$extensionConfiguration['temppasswordduration'];
		$this->tempblockloginduration  = (int)$extensionConfiguration['tempblockloginduration'];
		$this->pagecondition  = $extensionConfiguration['pagecondition'];
		if (substr($this->pagecondition,0,1)!='/') $this->pagecondition='/index.php?id='.$this->pagecondition;
		$this->pagehome  = $extensionConfiguration['pagehome'];
		if (substr($this->pagehome,0,1)!='/') $this->pagehome='/index.php?id='.$this->pagehome;
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer');	
		$this->pagemylogin  = $extensionConfiguration['pagemylogin'];
		if (substr($this->pagemylogin,0,1)!='/') $this->pagemylogin='/index.php?id='.$this->pagemylogin;
		$this->pagemy2FA  = $extensionConfiguration['pagemy2FA'];
		if (substr($this->pagemy2FA,0,1)!='/') $this->pagemy2FA='/index.php?id='.$this->pagemy2FA;
		if (!empty($_COOKIE["churchloginblocked"]))
			$this->blockedupto=$_COOKIE["churchloginblocked"];	
		elseif (!empty($_COOKIE["churchloginblockerrcnt"]))
		{
			$this->errcnt=(int)$_COOKIE["churchloginblockerrcnt"];
			if ($this->errcnt>6) {
				$this->errcnt=0;  // not blocked anay more
				$this->args["wwreden"]='';
			}
		}
		else 
		{
			if (isset($this->args['errorcount']))$this->errcnt = (int)$this->args['errorcount']; else $this->errcnt=0;
			//$this->errcnt=(int)$this->args['errorcount']; 
		}
    }

    /**
     * Show login form
     */
    public function loginAction(): ResponseInterface
    {

//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction args logintype: '.$this->logintype.'; user loggedin:'.$this->userAspect->isLoggedIn().', passwordstrength: '.$this->passwordstrength."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//$this->username=(string)$this->args['username'];
		//$this->passwordstrength=(string)$this->args['percentage'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction passwordstrength: '.$this->passwordstrength."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if ($this->isLogout()) {
			// apparently logoff
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction logout: '.$this->pagehome."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$this->fe_user->logoff();
			//header('Location: '.GeneralUtility::locationHeaderUrl($this->pagehome));
			return $this->responseFactory->createResponse(307)
            ->withHeader('Location', GeneralUtility::locationHeaderUrl($this->pagehome));
			//exit;
		}
		if (!empty($this->blockedupto))   // check pc blocked for login
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction COOKIE: '.$this->blockedupto."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			// session blocked for login:
			$this->error=['key'=> 'message.pcblock','var'=>$this->blockedupto];			
			$this->Logoff();
		}
		elseif ($this->hasLoginErrorOccurred()) {
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction WrongUseridPassword user:'.$this->username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$this->WrongUseridPassword();			
		}
		elseif ($this->wwreden>0 && $this->logintype === 'login')
		{
			$chuser=$this->fe_user->user;
			if (!$this->ProcessAuthenticationcode($chuser)	)
			{
				$this->Logoff();
			}
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction ga naar savepasswordAction wwreden: '.$this->wwreden."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

			if ($this->savepasswordAction())
			{
/*	            return (new ForwardResponse('overview'))->withArguments(['twoFA' => $this->twoFA]);
				ob_flush();
				header('Location: '.GeneralUtility::locationHeaderUrl($this->pagehome),true);
				//echo '<script type="text/javascript">location.reload(true);</script>';
				exit; */
	            //return (new ForwardResponse('overview'))->withArguments(['twoFA' => $this->twoFA]);
				return $this->responseFactory->createResponse(307)
		            ->withHeader('Location', GeneralUtility::locationHeaderUrl($this->pagehome));

			}
			$this->Logoff();
		}
		elseif ($this->logintype === 'login') 
		{	
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction CheckuseridPassword'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$this->errcnt=0;
			if ($this->passwordstrength <75)
			{
				$this->wwreden=4;
//				error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction paaswordchange passwordstrength: '.$this->passwordstrength."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			}
			elseif ($this->CheckuseridPassword())
			{
				// correct login:
				// goto homepage
//				error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction correct login:  user loggedin:'.$this->userAspect->isLoggedIn().'; headerpage:'.GeneralUtility::locationHeaderUrl($this->pagehome) ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
				
/*				if ($this->twoFA)
				{
	                return $this->redirectToUri(GeneralUtility::locationHeaderUrl($this->pagemy2FA));

					ob_flush();
					header('Location: '.GeneralUtility::locationHeaderUrl($this->pagemy2FA),true);
					exit; 
				}
				else */
//		            return (new ForwardResponse('overview'))->withArguments(['twoFA' => $this->twoFA]);
					return $this->responseFactory->createResponse(307)
		            ->withHeader('Location', GeneralUtility::locationHeaderUrl($this->pagehome));

	//header('Location: '.GeneralUtility::locationHeaderUrl($this->pagehome),true);
				//echo '<script type="text/javascript">location.reload(true);</script>';
//				exit;
			}
			$this->Logoff();
		}
		else
		{
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction show login form:'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			// No user currently logged in:
			//$this->errcnt=0;
			$this->statelogin = 'login';
		}
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction show new login form'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$language=$this->conf['lang'];
        $storagePageIds = ($GLOBALS['TYPO3_CONF_VARS']['FE']['checkFeUserPid'] ?? false)
            ? $this->pageRepository->getPageIdsRecursive(GeneralUtility::intExplode(',', (string)($this->settings['pages'] ?? ''), true), (int)($this->settings['recursive'] ?? 0))
            : [];

    	$assignedValues = [
			'statelogin' => $this->statelogin,
			'error' => $this->error,
			'errorcount' => $this->errcnt,
			'blockedupto' => $this->blockedupto,
			'language' => $language, 
			'username' => $this->username,
			'wwreden'=> $this->wwreden,
			'uid' => $this->uid,
			'passwordduration'=>$this->passwordduration,
			'passwordupdatetime'=>$this->passwordupdatetime,
            'requestToken' => RequestToken::create('core/user-auth/fe')
             ->withMergedParams(['pid' => implode(',', $storagePageIds)]),

        ];
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction show login error: '.urldecode(http_build_query($assignedValues['error'],NULL,"=")).'; assigendevalues: '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
        $this->view->assignMultiple($assignedValues);
		return $this->htmlResponse();
	} 

	    /**
     * User overview for logged in users
     */
    public function overviewAction(): ResponseInterface
    {
        if (!$this->userAspect->isLoggedIn()) {
            return new ForwardResponse('login');
        }

        $this->view->assignMultiple(
            [
                'user' => $this->fe_user,
            ]
        );

        return $this->htmlResponse();
    }

	
	 /**
     * Shows the recovery form. If $userIdentifier is set an email will be sent, if the corresponding user exists
     *
     * @throws StopActionException
     * @throws UnsupportedRequestTypeException
     */
    public function registerAction(): ResponseInterface
    {

		//$this->Account=(array)$this->args['account'];

		$Account= new Account();
		if (!empty($this->account))
		{
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'registerAction after error: '.urldecode(http_build_query($this->error,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		$websitename=explode('.',$_SERVER['SERVER_NAME'])[1]; 
			$Account->setVoornaam($this->account['voornaam']);
			$Account->setAchternaam($this->account['achternaam']);
			$Account->setGeboortedatum($this->account['geboortedatum']);
			$Account->setUsername($this->account['username']);
			$Account->setVoorwaarde($this->account['voorwaarde']);
		}
		//if (array_key_exists('statelogin',$this->args))$logintype = (string)$this->args['statelogin'];else $logintype='';

		$assignedValues = [
		'error' => $this->error,
		'statelogin' => $this->statelogin,
		'pagecondition' => $this->pagecondition,
		'account' => $Account,
     	];

        $this->view->assignMultiple($assignedValues);
		return $this->htmlResponse();

    }

	 /**
     * function saveccountAction
     *
     */
    public function saveaccountAction(): ResponseInterface
    {
		//$this->Account=(array)$this->args['account'];
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction Account: '.GeneralUtility::array2xml($this->account)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$this->voornaam = $this->account['voornaam'];
		$this->achternaam = $this->account['achternaam'];
		$this->geboortedatum = $this->account['geboortedatum'];
		$this->username = $this->account['username'];
		$geboortedatum = \DateTime::createFromFormat('d-m-Y', $this->geboortedatum);
		$geboortedat= $geboortedatum->format('Y-m-d');
		$websitename="Parousia Zoetermeer";
		
		// controleer of persoon uniek gevonden kan worden:
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction voornaamn: '.$this->voornaam."; achternaam;".$this->achternaam.'; geboortedat:'.$geboortedat.'; username:'.$this->username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	    $rows = $this->loginRepository->findRegisterdata($geboortedat,$this->voornaam,$this->achternaam);
		$aantalgevonden=count($rows);
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction aantal gevonden personen:'.$aantalgevonden."; personen;".GeneralUtility::array2xml($rows)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	
		if ($aantalgevonden<1)
		{
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction bijbehorende persoon niet gevonden'."; personen;".GeneralUtility::array2xml($rows)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			return($this->WrongRegistration(['key'=> 'registrate.namenotfound']));
		}
	
		if ($aantalgevonden>1)
		{	// check eerst op ingevulde geboortedatum:
			$row=$rows[0];
			if ($row["geboortedatum"]==$geboortedat or $row["geboortedatum"]=='0000-00-00')
			{	
				$email=$row["emailadres"];
				$naam=$row["naam"]; // maak naam volledig
				$id_persoon=$row["uid"];
			}
			else return($this->WrongRegistration(['key'=> 'registrate.morefound']));
		}
		else // precies 1 gevonden
		{	
			$row=$rows[0];
			$naam=$row["naam"]; // maak naam volledig
			$email=$row["emailadres"];
			$id_persoon=$row["uid"];
		}
		if (empty($email)) return($this->WrongRegistration(['key'=> 'registrate.noemail']));
		// check correcte geboortedatum:
		$curjaar=date("Y");
		if (substr($geboortedat,0,4)<($curjaar-120) or substr($geboortedat,0,4)>($curjaar-5))
			return($this->WrongRegistration(['key'=> 'registrate.tooold']));
		// vastleggen geboortedatum:
		$this->loginRepository->recordBirthdate($id_persoon,$geboortedat);
		 
	/*	// Check of user bij gemeente behoort:
		if ($row["bezoeker"]!=='is bezoeker')
		{	return "U behoort niet bij de gemeente en hebt dus geen toegang tot intranet";} */
		
		// controle of accountname al bestaat:
		$inUse=$this->loginRepository->checkAccountUsed($id_persoon,$this->username);
		if ($inUse)return($this->WrongRegistration(['key'=> 'registrate.used','var' => $this->username]));
		// vastleggen account:

		$result=$this->loginRepository->recordAccount($id_persoon,$this->username,$this->spid,$this->temppwduration);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction result recordaccount: '.GeneralUtility::array2xml($result)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$this->statelogin='finished';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction rstatelogin: '.$this->statelogin."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (isset($result['key']) and !empty($result['key']))return($this->WrongRegistration(['key'=> $result['key'],'var'=>$email]));
		$ww=$result['password'];
		if ($result['geblokkeerd'])
		{
			// reset blockades
			$time=time()+600;
			$timestr=date('H:i',$time);
			setcookie("churchloginblocked",$timestr,['expires' => time()-600,'domain' => $_SERVER['SERVER_NAME'] ] );
			setcookie("churchloginblockerrcnt]",0,['expires' => $time,'domain' => $_SERVER['SERVER_NAME'] ] );
			$this->errcnt=0;
		}
			
		// mail via noreply.parousiazoetermeer@gmail.com:
		$pagemylogin= GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer','pagemylogin');
	//	if (substr($mylogin,0,1)!='/') $mylogin='/index.php?id='.$mylogin;
		$timeTemp=strftime('%H:%M',strtotime("+ ".$this->temppwduration." minutes"));
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction maak email aan op: '.$timeTemp.'; voor:'.$naam."(".$this->username.")"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		$websitename=explode('.',$_SERVER['SERVER_NAME'])[1]; 
	    $assignedValues = [
			'name' => $naam,
			'websitename' => ucfirst($websitename),
			'username' => $this->username,
			'temporarypasword' => $ww,
			'timetemp' => $timeTemp,
			'pagemylogin' => (empty($_SERVER['HTTPS'])?"http":"https")."://".$_SERVER['SERVER_NAME'].$pagemylogin,
      		];

		$this->SendEmailFluid($naam,$email,$assignedValues,'NewUserAccount');

		// return to registration screen:
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction rstatelogin:2 '.$this->statelogin."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$assignedValues = [
			'error' => ['key'=> 'registrate.finished','var'=>$email,'var1'=>$timeTemp],
			'statelogin' => $this->statelogin,
      		];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaccountAction assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
        return (new ForwardResponse('register'))->withArguments($assignedValues);
    }
	
	 /**
     * function savepasswordAction
     *
     */
    public function savepasswordAction(): bool 
    {
		$chuser=$this->fe_user->user;
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction chuser: '.$chuser['username'].'; nwww:'.$this->NwPassword.'; wwreden: '.$this->wwreden."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if ($this->wwreden==1)
		{
			$DateTempPassw=new \DateTime($chuser["TimeTempPassword"]);
			$interval=new \DateInterval('PT'.$this->temppwduration.'M');
			$now= new \DateTime("");
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction now: '.$now->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$now->sub($interval);
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction now -'.(string)$this->temppwduration.'35m: '.$now->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction TimeTempPassword '.$DateTempPassw->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction nu groter '.json_encode(($DateTempPassw < $now))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			
			if ($DateTempPassw < $now)
			{
//				error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction LstInlog '.$DateTempPassw->format("Y-m-d H:i:s").' eerder dan now:; '.$now->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
				$this->error=['key'=> 'account.tempexpired'];
				return false;
			}
		}

		if( $chuser['geblokkeerd'])	{
			$this->error=['key'=> 'account.block'];
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction geblokeerd: '.$this->fe_user->user['username']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			return false;
	    }
		
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction niet geblokeerd: '.$this->fe_user->user['username']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
/*		$privkey='1000a123e233373c7174e73eb41a0114a6244dde3be117a2be230befb0aac81ce471';
		$RSA=new RSA();
		$decrtext=$RSA->DecryptRSA($this->NwPassword,$privkey);
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savepasswordAction ww nw: '.$decrtext."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (!$this->ProcessNewPassword($decrtext,$chuser)) return false; */
		if (!$this->ProcessNewPassword($this->NwPassword,$chuser)) return false;
		// correct login:
		$this->loginRepository->securityupdate(NULL,$chuser['person_id'],true,$this->fe_user);
		// redirect to homepage:
		//header('Location: '.GeneralUtility::locationHeaderUrl(''));
		//exit;
		return true;
    }

	//
	//	Send email fluid to user with new userid password
	//

	function SendEmailFluid($name,$email,$assignedValues,$templatename){
		// Prepare and send the message
		$mail= new FluidEmailReal();
		$mail
		   // Give the message a subject
		   ->from(new Address($GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'],'webmaster parousia zoetermeer'))
		   // Give it a body
		   ->format('html')
		   ->to(new Address($email, $name))
		   ->setTemplate($templatename)
		   ->assignMultiple($assignedValues)
		   ->send();
	}

	
    /**
     * Check if password and uaerid are correct
     */
	function WrongRegistration($error): ResponseInterface
	{
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'WrongRegistration error:'.GeneralUtility::array2xml($error)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'WrongRegistration statelogin: '.$this->statelogin."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	    $assignedValues = [
			'account' => $this->account,
			'error' => $error,
			'statelogin'=> $this->statelogin,
      		];
       // return new ForwardResponse('register');

        return (new ForwardResponse('register'))
			->withControllerName('Login')
			->withExtensionName('churchlogin')
			->withArguments($assignedValues); 

	}
    /**
     * Check if password and uaerid are correct
     */
	function CheckuseridPassword(): bool
	{
		// check if userid and password are correct:
		$contentU='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword : '.$this->fe_user->user['username']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$chuser=$this->fe_user->user;
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword chuser: '.GeneralUtility::array2xml($chuser)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		
		if (!empty($chuser["lastUpdPwd"]) and is_numeric($chuser["lastUpdPwd"]))
		{
			$lstupdate = \DateTime::createFromFormat("YmdHis",$chuser["lastUpdPwd"]) ;
			if ($lstupdate) $this->lstupd= $lstupdate->getTimestamp();
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword lastUpdPwd: '.$chuser["lastUpdPwd"].'; lstupd:'.$this->lstupd."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if ($chuser['geblokkeerd'])	{
			$this->error=['key'=> 'account.block','var'=>$this->username];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword geblokeerd: '.$this->fe_user->user['username']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			return false;
	    }
		
/*		    elseif ($chuser['bezoeker']=='is geen bezoeker')
		{	$this->errormsg.='<br>Uw behoort niet bij de gemeente en hebt dus geen toegang tot intranet';
			$this->fe_user->logoff();
			return;
		} */
		//check eerste keer of verlopen:
		$this->uid=$chuser['uid'];
		$this->username=$chuser['username'];
		$this->loginRepository->securityupdate(NULL,$chuser['person_id']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword securitylevel: '.$chuser["securitylevel"].'; huidiginloglevel: '.$chuser["huidiginloglevel"]."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		if (empty($this->lstupd))
		{	//	eerste keer aanloggen:
			$DateTempPassw=new \DateTime($chuser["TimeTempPassword"]);
			$interval=new \DateInterval('PT'.$this->temppwduration.'M');
			$now= new \DateTime("");
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword now: '.$now->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$now->sub($interval);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword now -35m: '.$now->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword TimeTempPassword '.$DateTempPassw->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			if ($DateTempPassw < $now)
			{
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword LstInlog '.$DateTempPassw->format("Y-m-d H:i:s").' eerder dan now:; '.$now->format("Y-m-d H:i:s")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
				$this->error=['key'=> 'account.tempexpired'];
				return false;
			}
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'loginAction before logoff uid: '.$this->fe_user->user['uid'].'; aspect id: '.$this->userAspect->get('id').'; aspect loggedin: '.$this->userAspect->isLoggedIn()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');			
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword eerste keer: '.$this->username.'; lstupdpw: '.$this->lstupd."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$this->wwreden=1;
			return false;

		}
/*		elseif ($chuser["securitylevel"]<>"licht" and $chuser["huidiginloglevel"]=="licht")
		{	
			// kennelijk zwaarder wachtwoord nodig
//				$this->lstupd=""; 
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword zwaarder password: '.$this->username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$this->wwreden=3;
			return false;
		} */
		elseif ((time()-$this->lstupd)>$this->passwordduration)   // wachtwoord verlopen?
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword wachtwoord verlopen: '.$this->username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			if ($chuser["securitylevel"]<>"licht") // licht level: wachtwoord verloopt niet
			{	if ((time()-$this->lstupd-$this->passwordduration)>$this->passwordupdatetime )  // wachtwoord definitief verlopen?
				{
					$this->error=['key'=> 'account.expired'];
					return false;
				}
				else
				{	
					$this->wwreden=2;
					return false;
				}
			}
		}
		// correct login:

		if (!$this->ProcessAuthenticationcode($chuser)	) return false;

		// Increase number of logins:
		$this->loginRepository->recordLogin($chuser['uid'],$chuser['AantalMaalIngelogd']);
		return true;
	}
	
	/**
	* Handle authentication code
	*/
	function ProcessAuthenticationcode($chuser, ): bool 
	{
		// validate autehntication code
		if (isset($this->googlepass))$authcode=$this->googlepass;else $authcode='';
		If ($chuser["tx_googleauth_enabled"] && !empty($chuser["tx_authenticator_secret"]) )
		{
            $authenticator = GeneralUtility::makeInstance(TokenAuthenticator::class);
			$authenticator->setUser( $this->fe_user);
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword verify authcode '.$authcode."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
            $postTokenCheck = $authenticator->verify(
                $chuser['tx_authenticator_secret'],
                (integer)$authcode
            );
            if (!$postTokenCheck) {
				$this->WrongUseridPassword();	
				$this->error=['key'=> 'message.authinvalid'];
				return false;
			}
		}
		return true;
	}

	/**
	* Handle new password
	*/
	function ProcessNewPassword($WachtwoordNw,$row): bool
	// afhandeling van ingevoerd nieuw wachtwoord:
	{	$aPrvww=array();
	//	wachtwoord gewijzigd:
		$huidiglevel=$row["securitylevel"];
	
		// check hoofd/kleine lettes en cijfers:
		if (!(preg_match ("#[A-Z]+#", $WachtwoordNw) and preg_match ("#[a-z]+#", $WachtwoordNw) and preg_match ("#[0-9]+#", $WachtwoordNw) and strlen($WachtwoordNw)>=8))
		{
			$this->error=['key'=> 'message.required','var' => $WachtwoordNw];
//			return "Nieuw wachtwoord (".$WachtwoordNw.") moet aan de volgende eisen voldoen:<ul><li> minimaal 8 tekens lang zijn,</li><li>zowel hoofd als kleine letters bevatten,</li><li>ook cijfers bevatten.</li></ul>"; //message.required+message.var
			return false;
		}
	
		// check ongelijk vorige wachtwoorden:
		$WachtwoordEncr=SHA1($WachtwoordNw);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'VerwerkNieuwwachtwoord ww nw: '.$WachtwoordNw.'; encr:'.$WachtwoordEncr.'; pswPrevious1 '.$row['pswPrevious1']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
						
		$aPrvww=array();
		if (!empty($row['pswPrevious1']))$aPrvww=explode(",",$row['pswPrevious1']);
		array_unshift($aPrvww,$row['password']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'VerwerkNieuwwachtwoord aPrvww: '.urldecode(http_build_query($aPrvww,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if ($key=array_search($WachtwoordEncr,$aPrvww)!==false)
		{	
			//return "Nieuw wachtwoord moet ongelijk een vorig wachtwoord zijn";
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ProcessNewPassword ww : '.$WachtwoordEncr.'; gevonden bij '.$key.': '.$aPrvww[$key]."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');			
			$this->error=['key'=> 'message.equalprev'];
			return false;

		}
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'VerwerkNieuwwachtwoord go to CheckGoodPassword person_id: '.$row["person_id"].'; user:'.$row["username"]."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		// check niet te gemakkelijk wachtwoord
		if (!$this->CheckGoodPassword($WachtwoordNw,$row["person_id"],$row["username"]))
			return false;
/*		if (!empty($this->error))
		{	return false;
		} */
		if (count($aPrvww)>20)array_pop($aPrvww); // limiteer op 20 vorige wachtwoorden
		// update password:
		// update user table
		$this->loginRepository->UpdateUserTable($WachtwoordEncr,$aPrvww,$huidiglevel,$row['uid']);
		
		return true;
	}
	
	/**
	* Check if new password complies standards
	*/
	function CheckGoodPassword($nwwachtw,$id_person,$login): bool
	{	// check niet te gemakkelijk wachtwoord
		// check niet allemaal dezelfde letters:
		$nwwachtw=strtoupper($nwwachtw);
		$aLet=preg_split('//',$nwwachtw, -1, PREG_SPLIT_NO_EMPTY);
		// bepaal eerst alphanumerieke en numerieke deel
		$wwalpha="";
		$wwnum="";
		foreach ($aLet as $let) {if (ctype_alpha($let))$wwalpha.=$let;else $wwnum.=$let;}
		asort($aLet);
		$tel=0;$cur="";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckGoodPassword tel aantal verschillende letters wwalpha: '.$wwalpha.'; wwnum: '.$wwnum."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		// tel aantal verschillende letters
		foreach ($aLet as $let) {if ($let<>$cur){$tel++;$cur=$let;}}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckGoodPassword aantal verschillende letters tel: '.$tel."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if ($tel<4) 
		{
			//return "Nieuw wachtwoord moet minimaal 4 verschillende letters bevatten"; //message.divers
			$this->error=['key'=> 'message.divers'];
			return false;
		}
		// check geen al te simpel wachtwoord
		if (
			(strlen($wwnum)>3 and strpos("01234567890",$wwnum)!==false) or 
			(strlen($wwalpha)>3 and strpos("ABCDEFGHIJKLMNOP",$wwalpha)!==false) or
			$nwwachtw==strtoupper($login) or
			$wwalpha==strtoupper($login) or 
			strpos($nwwachtw,'PAROUSIA')!==false
			)
		{
			$this->error=['key'=> 'message.simple'];
			return false;
		}
		// vergelijk wachtwoord met gegevens uit de database van de persoon:
		$this->error=['key'=> 'message.passknown'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckGoodPassword findpersondata id_person: '.$id_person."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	    $rows = $this->loginRepository->findPersondata($id_person);
		$pct=0;
		foreach ($rows as $row)
		{	
			$aStr=preg_split("/[\s-]+/",$row['personstring'],0,PREG_SPLIT_NO_EMPTY);
			foreach ($aStr as $pstr)
			{
				similar_text($nwwachtw,$pstr,$pct);
				if($pct>75)return false;
				if (strlen($wwalpha)>3)
				{
					similar_text($wwalpha,$pstr,$pct);
					if($pct>75)return false;
				}
				if (strlen($wwnum)>3)
				{
					similar_text($wwnum,$pstr,$pct);
					if($pct>75)return false;
				}
			}
		}
		// Good password: clear error
		$this->error=array(); 
		return true; 
	}	

    /**
     * Handle incorrect login:
     */
	function WrongUseridPassword(): void
	{
		$this->statelogin=$this->logintype;
		$this->errcnt++;
		$authInfo = $this->fe_user->getAuthInfoArray($this->request);
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'churchlogin authinfo: '.urldecode(http_build_query($authInfo,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$chuser=$this->loginRepository->fetchUserRec($authInfo['db_user'],$this->username);
		if (is_array($chuser))
		{
			$this->uid=$chuser["uid"];
			$this->errcntUser=$chuser['LoginErrorCount']+1;
		}
		//$loginData = $this->fe_user->getLoginFormData();
		//$this->uid=GeneralUtility::_GP('uid');
		//if (is_array($chuser))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'churchlogin chuser: '.GeneralUtility::array2xml($chuser)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		setcookie('roepnaam','', time() - 3600);
		setcookie('fe_typo_user','', time() - 3600);
/*		if ($this->errcnt==1)
			if (is_array($chuser)) $this->uid=$chuser["uid"];
		else
			if (empty($this->uid) || !is_array($chuser) || $this->uid!==$chuser["uid"])
				$this->uid="";
			elseif (is_array($chuser)) $this->uid=$chuser["uid"]; */
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'WrongUseridPassword foutief username: '.$this->username.'; uid:'.$this->uid.'; errcntUser: '.$this->errcntUser."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$this->fe_user->setAndSaveSessionData('person_id', null);
		$this->fe_user->setAndSaveSessionData('uid', null);
		$this->fe_user->user['uid']=0;
		$this->fe_user->setAndSaveSessionData('username', null);
		$this->fe_user->setAndSaveSessionData('name', null);
		//$GLOBALS['TSFE']->loginUser=false;
		if ($this->errcntUser > 0)  // thus chuser filled
		{
			if ($this->errcntUser > 4)
			{
				// block user:
				$this->error=['key'=> 'account.block','var'=>$this->username];
				$this->loginRepository->blockuser($this->uid);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'churchlogin user blocked uid: '.$this->uid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			}
			else 			//update errorcount user:
				$this->loginRepository->recordLoginError($this->uid,$this->errcntUser);
		}
		$time=time()+$this->tempblockloginduration*60;
		$timestr=date('H:i',$time);
		if ($this->errcnt<7) 
		{
			setcookie("churchloginblockerrcnt",$this->errcnt,['expires' => $time,'domain' => $_SERVER['SERVER_NAME'] ] );
			if (empty($this->error))$this->error=['key'=> 'message.wrong','var' => (7-$this->errcnt)];
		}
		else
		{
			// block login from this terminal for 10 minutes
			setcookie("churchloginblocked",$timestr,['expires' => $time,'domain' => $_SERVER['SERVER_NAME'] ] );
			setcookie("churchloginblockerrcnt]",0,['expires' => $time,'domain' => $_SERVER['SERVER_NAME'] ] );
		//	$this->errcnt=0;
			if (empty($this->error))$this->error=['key'=> 'message.pcblock','var'=>$timestr];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'churchlogin terminal blocked at:'.$timestr."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		}
			 
		return;
	}	
	

	/**
	* Perform Logoff
	*/
	function Logoff()
	{
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Logoff en terug naar loginscherm'.'; user:'.$this->username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$this->fe_user->logoff();
		setcookie('fe_typo_user','', time() - 3600);
		//$challenge = md5(uniqid('').getmypid());
	//	$this->fe_user->setAndSaveSessionData('login_challenge', $challenge);
		$language=$GLOBALS['TSFE']->config['config']['language'];
    	$assignedValues = [
			//'statelogin' => $this->statelogin,
			'error' => $this->error,
            //'challenge' => $challenge ,
			'errorcount' => $this->errcnt,
			'blockedupto' => $this->blockedupto,
			'language' => $language, 
			'username' => $this->username,
			'wwreden'=> $this->wwreden,
			'uid' => $this->uid,
			'passwordduration'=>$this->passwordduration,
			'passwordupdatetime'=>$this->passwordupdatetime,
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Logoff assigned values : '.urldecode(http_build_query($assignedValues['error'],NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$this->redirect('login',null,null,$assignedValues);
	}
	
	
    protected function isLogout(): bool
    {
        return empty($this->logintype) && $this->userAspect->isLoggedIn();
    }

    protected function hasLoginErrorOccurred(): bool
    {
        return $this->logintype === 'login' && !$this->userAspect->isLoggedIn();
    }
}
