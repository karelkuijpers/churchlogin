<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
namespace Parousia\Churchlogin\Controller;

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use Tx\Authenticatorfe\Auth\TokenAuthenticator;
use Parousia\Churchlogin\Domain\Repository\LoginRepository;
use Psr\Http\Message\ResponseInterface;


/**
 * Used for plugin login
 */
class TwofactorController extends ActionController
{
	
    /**
     * @var LoginRepository
     */
    protected $loginRepository;

	protected $args = array();
	
    /**
     * @var UserAspect
     */
    protected $userAspect;

    /**
     * @var AbstractUserAuthentication
     */
    protected $fe_user = null;

    public function __construct(
        LoginRepository $loginRepository
    ) {
        $this->loginRepository = $loginRepository;		
        $this->userAspect = GeneralUtility::makeInstance(Context::class)->getAspect('frontend.user');
    }

    /**
     * Initialize redirects
     */
    public function initializeAction(): void
    {
		$this->args=$this->request->getArguments();
		//$this->fe_user = $GLOBALS['TSFE']->fe_user;
		$this->fe_user = $this->request->getAttribute('frontend.user');

    }

    /**
     * Show login form
     */
    public function enable2FAAction(): ResponseInterface
    {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'enable2FAAction args : '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		$enabled= $this->fe_user->user['tx_googleauth_enabled'];
		if (!empty($this->args))
		{
			if (array_key_exists ( 'authenticator-enabled',$this->args))
			{
				$enabled=$this->args['authenticator-enabled'];
				$this->loginRepository->record2FAEnabled($this->fe_user->user['uid'],$enabled);
			}
			// redirect to home:
			$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchlogin');	
			header('Location: '.GeneralUtility::locationHeaderUrl($extensionConfiguration['pagehome']));
			exit;
		}

		$authenticator = GeneralUtility::makeInstance(TokenAuthenticator::class);
		$authenticator->setUser( $this->fe_user);


        // Set random secret if empty
        if (trim($this->fe_user->user['tx_authenticator_secret']) == '') {
            $authenticator->createToken('TOTP');
        }

        $label = $this->fe_user->user[$this->fe_user->username_column].'-'.$GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'];
        $authUrl = $authenticator->createUrlForUser($label);
        $data = $authenticator->getData();

    	$assignedValues = [
            'authUrl' => $authUrl ,
			'tokenKey' => $data['tokenkey'],
			'authenticator-enabled' => $enabled,
        ];
        $this->view->assignMultiple($assignedValues);
		return $this->htmlResponse();
	}
	 
	
	
}
