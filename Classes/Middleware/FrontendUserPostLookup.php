<?php
namespace Parousia\Churchlogin\Middleware;
use Parousia\Churchlogin\Hooks\churchlogin_div;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Core\Utility\GeneralUtility;

ini_set("display_errors",1);
ini_set("log_errors",1);
mb_internal_encoding("UTF-8");

/*
 * This file is part of the TYPO3 ChurchLogin project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 */


/**
 * This middleware authenticates a Frontend User (fe_users).
 * A valid $GLOBALS['TSFE'] object is needed for the time being, being fully backwards-compatible.
 */
class FrontendUserPostLookup implements MiddlewareInterface
{
    /**
     * Add permission data to a frontend authenticated user
     * as found in the object $this->fe_user.
	 8 it should be called after middleware TYPO3\CMS\Frontend\Middleware\FrontendUserAuthenticator
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
	 
	var $fe_user;
	
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
 		$context = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class);

        $frontendUser = $context->getAspect('frontend.user');
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'middelware PostUserLookUp process ingelogd : '.$frontendUser->get('isLoggedIn')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');

		if ($frontendUser->get('isLoggedIn')>0) 
		{
			$this->fe_user = $request->getAttribute('frontend.user');
			$connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('persoon');
			$result = $connection->executeQuery('SELECT @password:="'.$GLOBALS['TYPO3_CONF_VARS']['PARCHURCH']['Connections']['parsl'].'"');
			$result = $connection->executeQuery("set session sql_mode = ''");
			$result = $connection->executeQuery("SET group_concat_max_len=50000;");

			$statement='SELECT concat_ws(" ",roepnaam,tussenvoegsel,achternaam) as name,roepnaam, lid,gedoopt, bezoeker,AES_DECRYPT(emailadres,@password) as emailadres'.
			',if(datediff(curdate(),LstInlog)=0,"","1") as nwlogin,LstInlog,person_id,securitylevel'.
			' From persoon,fe_users'.
			' WHERE fe_users.uid = '.$frontendUser->get('id').' and persoon.uid = fe_users.person_id AND persoon.deleted=0';
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'middelware PostUserLookUp getuser statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
			$user = $connection->executeQuery($statement)->fetch();
			if (is_array($user))
			{
//				error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'middelware PostUserLookUp user: '.GeneralUtility::array2xml($user)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
				$this->fe_user->user['name']=$user['name'];
				$this->fe_user->user['roepnaam']=$user['roepnaam'];
				$this->fe_user->user['gedoopt']=$user['gedoopt'];
				$this->fe_user->user['lid']=$user['lid'];
				$this->fe_user->user['bezoeker']=$user['bezoeker'];
				$this->fe_user->user['email']=$user['emailadres'];
				$this->fe_user->user['nwlogin']=$user['nwlogin'];
				$this->fe_user->user['profielalert']='0'; // first login today
				$this->fe_user->user['person_id']=$user['person_id']; // first login today
//				error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'middelware PostUserLookUp feuser->user: '.GeneralUtility::array2xml($this->fe_user->user)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
/*				if ($user['nwlogin'] > '0')
				{ // read bedieningsprofiel:
					$statement='SELECT uid,AES_DECRYPT(verlangen,@password) as verlangen'.
						',AES_DECRYPT(gaven,@password) as gaven'.
						',AES_DECRYPT(wat_aanspreekt,@password) as wat_aanspreekt'.
						',AES_DECRYPT(anders,@password) as anders '.
						'FROM bedieningsprofiel '.
						'WHERE AES_DECRYPT(bedieningsprofiel.person_id,@password) = '.(string)$user['person_id'].' AND bedieningsprofiel.deleted=0' ;
					$opm = $connection->executeQuery($statement)->fetch();
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'middelware PostUserLookUp statment: '.$statement.'; opm: '.GeneralUtility::array2xml($opm)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
					if (is_array($opm))
					{	// partly filled?
						if (empty($opm['verlangen']) || empty($opm['gaven']) || (empty($opm['wat_aanspreekt']) && empty($opm['anders'])))
							$this->fe_user->user['profielalert']='1';
					}
					else $this->fe_user->user['profielalert']='2'; // not filled at all					
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'middelware PostUserLookUp profielalert: '.$this->fe_user->user['profielalert']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
				} */
				if ( ! session_id() ) @ session_start();
				$this->fe_user->setKey('user','g_naam',$frontendUser->get('username'));
				$this->fe_user->setKey('user','adviseurs_id',$user['person_id']);
				$this->fe_user->setKey('user','feuserid',$frontendUser->get('id'));
				$this->fe_user->setKey('user','userid',$user['person_id']);
				
				// update LstLogin:
                $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
                $queryBuilder->getRestrictions()->removeAll();
                $queryBuilder->update('fe_users')
                     ->set('Lstinlog', (new \DateTime())->format('Y-m-d'))
                     ->where(
                          $queryBuilder->expr()->eq(
                                'uid',
                                $frontendUser->get('id')
                                )
                          )
                     ->execute(); 
				$seclevel=$user['securitylevel'];
				if ($this->fe_user->user['tx_googleauth_enabled']==0 && $user['securitylevel']=='zwaar') $seclevel='middel';
				$this->InitPermissions($frontendUser->get('id'),$frontendUser->get('username'),$user['person_id'],$seclevel);
			}
			else {
				$this->fe_user->user['name']='';
			}
			//$feuser=print_r($this->fe_user,true);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'middelware PostUserLookUp tsfe fe_user: '.$feuser."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		}
		return $handler->handle($request);	
	}

	function InitPermissions ($login,$loginnaam,$person_id,$seclevel="licht")
	{	// bepaal array met toegestane functionaliteiten en array met toegestande werkstations
		//Inlogaccount op de website
		$this->fe_user->setKey('user','permissie','');
		$this->fe_user->setKey('ses','permissie','');

		$_SESSION["g_naam"]=$loginnaam;
		$_SESSION["adviseurs_id"]=$person_id;
		$_SESSION["userid"]=$person_id;
		$_SESSION["feuserid"]=$login;
		$_SESSION["permissie"]=""; 
		$_SESSION["securitylevel"]=$seclevel;
		$TwoFA=$this->fe_user->user['tx_googleauth_enabled'];;
		$_SESSION['tx_googleauth_enabled']=$TwoFA;
		$this->fe_user->setKey('ses','tx_googleauth_enabled',$TwoFA);
		$this->fe_user->setKey('user','tx_googleauth_enabled',$TwoFA);
		$this->fe_user->setKey('ses','securitylevel',$seclevel);
		setcookie("roepnaam", $this->fe_user->user['roepnaam'],time()+60);
		//Rechten ophalen van gebruiker
	
		$statement='SELECT distinct lower(f.naam)as naam FROM rm_functionaliteit f, rm_gg_f as gg_f WHERE f.f_uid=gg_f.f_uid AND '.
		"f.securitylevel<='".$seclevel."' AND ".
		'FIND_IN_SET(gg_f.gg_uid,(select usergroup from fe_users where uid="'.$login.'")) order by f.naam;';
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('rm_functionaliteit');
		$rows = $connection->executeQuery($statement)->fetchAllAssociative();
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF']." FrontendUserPostLookup: InitPermissions: ".$statement."; error:"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');

		if (is_array($rows) and count($rows)>0)
		{	
			$permissies=array();
			foreach ($rows as $row)
			{	array_push($permissies,$row["naam"]);}
//			if (is_array($permissies))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": FrontendUserPostLookup".'permissies : '.urldecode(http_build_query($permissies,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');

			$_SESSION["permissie"]=$permissies;
			$this->fe_user->setKey('ses','permissie',$permissies);
			$this->fe_user->setKey('user','permissie',$permissies);
		} 
		

    }

}
