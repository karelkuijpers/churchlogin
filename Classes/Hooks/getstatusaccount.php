<?php
namespace Parousia\Churchlogin\Hooks;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;

class getstatusaccount 
{
	protected $db;

/**
 * @param ServerRequestInterface $request
 * @param ResponseInterface $response
 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		/*
		* file for ajax to check if a person has 2FA enabled and if password change is needed
		 * Post parameters: 
		 	username,
		 */
	    
		//$person_id=69;
		$response = GeneralUtility::makeInstance(Response::class);
		$aParms=$request->getParsedBody(); 
		$twoFA='';
		$wwreden=0;
		$uname='';
		$uid=0;
		$username='';
		$passwordduration=365;
		if (isset($aParms["username"])) $username=$aParms["username"];
		else die("You are not privileged to perform this action");
		if (isset($aParms["passwordduration"]))$passwordduration=$aParms["passwordduration"];
		if (isset($aParms["passwordupdatetime"]))$passwordupdatetime=$aParms["passwordupdatetime"];
		churchpersreg_div::connectdb($this->db);
		$query='select uid,lastUpdPwd,tx_googleauth_enabled,securitylevel,huidiginloglevel,username from fe_users where username="'.$this->db->real_escape_string(mb_strtolower($username)).'"';
		$result=$this->db->query($query) or die("Can't perform Query");	
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getstatusaccount query:"'.$query.'"; error:'.$this->db->error.'; aantal:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		If (empty($this->db->error) and $result->num_rows == 1)
		{
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getstatusaccount get results query'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
			$row=$result->fetch_array(MYSQLI_ASSOC);
			$enabled=$row['tx_googleauth_enabled'];
			$securitylevel=$row['securitylevel'];
			$uid=$row['uid'];
			$uname=$row['username'];
			$lstupd=0;
			if (!empty($row["lastUpdPwd"]) and is_numeric($row["lastUpdPwd"]))
			{
				$lstupdate = \DateTime::createFromFormat("YmdHis",$row["lastUpdPwd"]) ;
				if ($lstupdate) $lstupd= $lstupdate->getTimestamp();
			}
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Getstatus account wachtwoord verlopen: lstupd: '.strval($lstupd).'; verloop: '.strval(time()-$lstupd).'; passwordduration: '.strval($passwordduration).'; passwordupdatetime: '.strval($passwordupdatetime)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
			
			if (empty($lstupd))$wwreden=1;
		//	elseif (isset($row["securitylevel"]) and $row["securitylevel"]<>"licht" and isset($row["huidiginloglevel"]) and $row["huidiginloglevel"]=="licht")$wwreden=3;
			elseif ((time()-$lstupd)>$passwordduration)   // wachtwoord verlopen?
			{
//				error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'CheckuseridPassword wachtwoord verlopen: '.$this->username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
				if (isset($row["securitylevel"]) and $row["securitylevel"]<>"licht") // licht level: wachtwoord verloopt niet
				{	if ((time()-$lstupd-$passwordduration)<$passwordupdatetime )  // wachtwoord niet efinitief verlopen?
					{
						$wwreden=2;
					}
				}
			}

			$twoFA=''; // no 2FA
			if ($enabled)$twoFA="1";  //2FA enabled
			elseif (isset($row["securitylevel"]) and $securitylevel=="zwaar") $twoFA="2"; // 2FA needed
		}
		$data= array(
		'authenabled'=>$twoFA,
		'uid'=>$uid,
		'username'=>$uname,
		'wwreden'=>$wwreden);
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getstatusaccount data : '.urldecode(http_build_query($data,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		$response->getBody()->write(json_encode($data));
		return $response;
	}
}

 

