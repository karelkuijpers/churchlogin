<?php
namespace Parousia\Churchlogin\Hooks;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Authentication\AbstractAuthenticationService;
/***************************************************************
*  Copyright notice
*
*  (c) 2008 k.kuijpers <karelkuijpers@gmail.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/


/**
 * Service "User authentication church " for the "churchlogin" extension.
 *
 * @author	k.kuijpers <karelkuijpers@gmail.com>
 * @package	TYPO3
 */

class tx_churchlogin_sv1 extends AbstractAuthenticationService {

	/*	
	 * Find a user (eg. look up the user record in database when a login is sent)
	 *
	 * @return	mixed		user array or false
	 */
	function getUser()	{
		$user = false;
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Login getUser: '.GeneralUtility::array2xml($this->login)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'');
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getUser login uident: '.$this->login['uident'].'; uident-text:'.$this->login['uident_text'].'; uname:'.$this->login['uname']."; logintype:".$this->pObj->loginType."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log');

		if ($this->login['status']=='login' && ($this->login['uident'] || $this->login['uident_text']))	{
			if ($this->pObj->loginType == 'BE')
			{	// first try to find fe-user:
				$connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('be_users');
				$statement='SELECT be.*, fe.password as passwordFE FROM be_users be,fe_users as fe '.
				'WHERE be.tx_churchadmin_fe_user_id=fe.uid and fe.username="'.$this->login['uname'].'"';
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getUser'.$this->pObj->loginType.' statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log');
				$users = $connection->executeQuery($statement)->fetchAll();
				if (is_array($users) and count($users)==1)
				{	
					$user = $users[0];
					$user["username"]=$this->login['uname'];
					// enforce comparibility of passwords within SaltedPasswordService: passwordFE is allready stored as SHA1(password) , uident_text contains decrypted password
					$user["password"]=$user["passwordFE"];
				}
				else 
				{	
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetUser BE fetchUserRecord username'.$this->login['uname']."; dbuser:".GeneralUtility::array2xml($this->db_user)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
				
					$user = $this->fetchUserRecord($this->login['uname']);

				}
			}
			else 
			{	
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetUser FE fetchUserRecord username'.$this->login['uname']."; dbuser:".GeneralUtility::array2xml($this->db_user)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log');
				
				$user = $this->fetchUserRecord($this->login['uname']);
			}
			if(!is_array($user)) {
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'No User found when reading GetUser '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.ltxt');

				// Failed login attempt (no username found)
				$this->writelog(255,3,3,2,
					"Login-attempt from %s (%s), username '%s' not found!!",
					Array($this->authInfo['REMOTE_ADDR'], $this->authInfo['REMOTE_HOST'], $this->login['uname']));	// Logout written to log
			}
			//else {error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'User found when reading GetUser'.$this->pObj->loginType.': '.$user['username']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			
		}
/*		if ($user)
			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'User found GetUser: '.$user['username'].'; pw:'.$user['password']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log');
		else error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'No User found GetUser '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log'); */

		return $user;
	}

	/**
	 * Authenticate a user (Check various conditions for the user that might invalidate its authentication, eg. password match, domain, IP, etc.)
	 *
	 * @param	array		Data of user.
	 * @return	boolean
	 */
	function authUser(array $user):int	{
		$OK = 100;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'authUser user: '.$user['username']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Begin authUser user: '.$user['username'].' , login:'.GeneralUtility::array2xml($this->login)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		if (($this->login['uident'] || $this->login['uident_text']) && $this->login['uname'])	{

			// Checking password match for user:

//			$OK = $this->comparePwds($user, $this->login,'superchallenged');
			$OK = $this->comparePwds($user, $this->login);

			if(!$OK)     {
				// Failed login attempt (wrong password) - write that to the log!
				if ($this->writeAttemptLog) {
					$this->writelog(255,3,3,1,
						"Login-attempt from %s (%s), username '%s', password not accepted!",
						Array($this->authInfo['REMOTE_ADDR'], $this->authInfo['REMOTE_HOST'], $this->login['uname']));
				}
			}

		}
		if ($OK)
		{	
			return 200;}
		if ($this->pObj->loginType == 'BE')  return 100; else return 0;   // continue with other service in case of BE
	}

	
	/* Check the login data with the user record data for builtin login methods
	 *
	 * @param	array		user data array
	 * @param	array		login data array
	 * @return	boolean		true if login data matched
	 */
	function comparePwds(array $user, array $loginData) {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."compareUident  type: ".$this->pObj->loginType.'; user pw:'.$user['password'].'; login uident_text: '.$loginData['uident_text'].'; sha text:'.sha1($loginData['uident_text']).'; loginData: '.GeneralUtility::array2xml($loginData)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log');
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'compareUident user password: '.$user['password'].' login[uident_text]:'.$loginData['uident_text']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'compareUident user password: '.$user['password'].' login[uident]:'.sha1($loginData['uident_text']).' login[uident_text]:'.$loginData['uident_text']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if ($user['password']==sha1($loginData['uident_text']))  // rsa security for BE and FE-user?
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'compareUident userpassword gelijk aan login password'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			return true;
		}
		else return false;
	} 

}

