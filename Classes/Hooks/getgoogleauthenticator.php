<?php
namespace Parousia\Churchlogin\Hooks;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;

class getgoogleauthenticator 
{
	protected $db;

/**
 * @param ServerRequestInterface $request
 * @param ResponseInterface $response
 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		/*
		* file for ajax to if a person has 2FA enabled
		 * Post parameters: 
		 	username,
		 */
	    
		//$person_id=69;
		$response = GeneralUtility::makeInstance(Response::class);
		$aParms=$request->getParsedBody(); 
		if (isset($aParms["username"])) $username=$aParms["username"];
		else die("You are not privileged to perform this action");
		
		churchpersreg_div::connectdb($this->db);
		$query='select tx_googleauth_enabled,securitylevel from fe_users where username="'.$this->db->real_escape_string(mb_strtolower($username)).'"';
		$result=$this->db->query($query) or die("Can't perform Query");	
		$row=$result->fetch_array(MYSQLI_ASSOC);
		$enabled=$row['tx_googleauth_enabled'];
		$securitylevel=$row['securitylevel'];
		$html=''; // no 2FA
		if ($enabled)$html="1";  //2FA enabled
		elseif ($securitylevel=="zwaar") $html="2"; // 2FA needed
		$response->getBody()->write($html);
		return $response;
	}
}

 

