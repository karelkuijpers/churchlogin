
#
# Table structure for table 'be_users'
#
CREATE TABLE `be_users` (
  `tx_churchadmin_fe_user_id` blob NOT NULL,
)  DEFAULT CHARACTER SET utf ;

#
# Table structure for table 'fe_users'
#
CREATE TABLE `fe_users` (
  `geblokkeerd` tinyint(1) DEFAULT NULL,
  `lastUpdPwd` varchar(14) DEFAULT NULL,
  `updFreqency` int(6) DEFAULT NULL,
  `pswPrevious1` blob,
  `person_id` int(11) DEFAULT NULL,
  `securitylevel` enum('licht','middel','zwaar') NOT NULL DEFAULT 'zwaar',
  `huidiginloglevel` enum('licht','middel','zwaar') NOT NULL DEFAULT 'licht',
  `LstInlog` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AantalMaalIngelogd` int(11) NOT NULL DEFAULT '0',
  `TimeTempPassword` datetime DEFAULT NULL,
  `LoginErrorCount` int(11) NOT NULL DEFAULT '0',
  `tx_googleauth_enabled` smallint NOT NULL DEFAULT '0',
  `tx_authenticator_secret` tinytext,  
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `fe_cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
)  DEFAULT CHARACTER SET utf8 ;

#
# Table structure for table 'persoon'
#
CREATE TABLE `persoon` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `achternaam` varchar(50) NOT NULL DEFAULT '',
  `tussenvoegsel` varchar(10) NOT NULL DEFAULT '',
  `voornamen` varchar(32) NOT NULL DEFAULT '',
  `roepnaam` varchar(20) NOT NULL DEFAULT '',
  `titel` varchar(20) NOT NULL DEFAULT '',
  `emailadres` tinyblob NOT NULL,
  `geslacht` enum('man','vrouw') DEFAULT NULL,
  `geboortedatum` date NULL DEFAULT NULL,
  `lid` enum('is lid','is geen lid') DEFAULT NULL,
  `datum_lidmaatschap` date NULL DEFAULT NULL,
  `id_adres` tinyblob,
  `steller` int(11) NOT NULL DEFAULT '0',
  `datum_wijziging` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 #ON UPDATE CURRENT_TIMESTAMP,
  `invoerdatum` date NULL DEFAULT NULL,
  `cleanteam` enum('kan meedoen','kan niet meedoen') NOT NULL DEFAULT 'kan meedoen',
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
)  DEFAULT CHARACTER SET utf8 ;


CREATE TABLE `rm_functionaliteit` (
  `f_uid` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(50) DEFAULT NULL,
  `omschrijving` varchar(255) DEFAULT NULL,
  `securitylevel` enum('licht','middel','zwaar') NOT NULL DEFAULT 'zwaar',
  PRIMARY KEY (`f_uid`)
)  DEFAULT CHARACTER  SET utf8;

#
# Table structure for table 'rm_gg_f'
#
CREATE TABLE `rm_gg_f` (
  `gg_uid` int(11) NOT NULL,
  `f_uid` int(11) NOT NULL,
  PRIMARY KEY (`gg_uid`,`f_uid`),
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;


