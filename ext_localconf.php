<?php
defined('TYPO3') or die();


// Add additional TypoScript & TsConfig 
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Churchlogin',
    'Twofactor',
    [
		\Parousia\Churchlogin\Controller\TwofactorController::class => 'enable2FA'
    ],
    [
		\Parousia\Churchlogin\Controller\TwofactorController::class => 'enable2FA'
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Churchlogin',
    'Login',
    [
        \Parousia\Churchlogin\Controller\LoginController::class => 'login, register,passwordchange,savepassword,saveaccount',
    ],
    [
        \Parousia\Churchlogin\Controller\LoginController::class => 'login, register,passwordchange,savepassword,saveaccount',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addService('churchlogin',  'auth' ,  'tx_churchlogin_sv1' ,
	array(

		'title' => 'User authentication church ',
		'description' => 'Authentication with username/password superchallenged',

		'subtype' => 'getUserBE,authUserBE,getUserFE,authUserFE,getGroupsFE',

		'available' => TRUE,
		'priority' => 95,
		'quality' => 80,
		'os' => '',
		'exec' => '',

		'className' => \Parousia\Churchlogin\Hooks\tx_churchlogin_sv1::class,
	)
);
$GLOBALS['TYPO3_CONF_VARS']['FE']['checkFeUserPid']=0;
//$GLOBALS['TYPO3_CONF_VARS']['FE']['loginSecurityLevel'] = 'superchallenged';
//$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['getgoogleauthenticator'] = \Parousia\Churchlogin\Hooks\getgoogleauthenticator::class .'::processRequest';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['getstatusaccount'] = \Parousia\Churchlogin\Hooks\getstatusaccount::class .'::processRequest';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][700] = 'EXT:churchlogin/Resources/Private/Templates/Email';

