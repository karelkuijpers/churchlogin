/*
 *  passtrength - v1.0.0
 *  passtrength
 *  passtrength
 *
 *  Made by @adrisorribas
 *  Under MIT License
 */
;(function($, window, document, undefined) {

  var pluginName = "passtrength",
      defaults = {
        minChars: 8,
        passwordToggle: true,
        tooltip: true,
		bar: true,
        textWeak: "Weak",
        textMedium: "Medium",
        textStrong: "Strong",
        textVeryStrong: "Very Strong",
		textChar: "chars",
      };

  function Plugin(element, options){
    this.element = element;
    this.$elem = $(this.element);
    this.options = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    _this      = this;
    this.init();
  }
  // $element.prop( "onclick", null );
  Plugin.prototype = {
    init: function(){
      var _this    = this,
          meter    = jQuery("<div/>", {class: "passtrengthMeter"}),
          tooltip = jQuery("<div/>", {class: "tooltip", text: "Min " + this.options.minChars + " " + this.options.textChar});

	  meter.insertAfter(this.element);
	  $(this.element).appendTo(meter);
      if(this.options.tooltip){
        tooltip.appendTo(meter);
        var tooltipWidth = tooltip.outerWidth() / 2;
        tooltip.css("margin-left", -tooltipWidth);
      }

      this.$elem.bind("paste", function(e) {
          value = e.originalEvent.clipboardData.getData('text');
          _this.check(value);
      });

      this.$elem.bind("keyup keydown", function() {
          value = $(this).val();
          _this.check(value);
      });

      if(this.options.passwordToggle){
        var buttonShow = jQuery("<span/>", {class: "showPassword fa fa-eye-slash"});
     	buttonShow.appendTo($(this.element).closest(".passtrengthMeter"));
		buttonShow.bind("click",function(e){
		  //$(".showPassword").click(function(){
		  	var inputfld= $(this).siblings('input');
			//alert("click on "+inputfld.attr("name")+"type:"+inputfld.attr('type'));
		  	if(inputfld.attr('type')=='password'){
				inputfld.attr('type','text');
				$(this).removeClass( "fa-eye-slash" );
				$(this).addClass( "fa-eye" );
			}
			else{
				inputfld.attr("type","password");
				$(this).removeClass( "fa-eye" );
				$(this).addClass( "fa-eye-slash" );
			}
		  }); 
      }

    },

    check: function(value){
      var secureTotal  = 0,
          chars        = 0,
          capitals     = 0,
          numbers      = 0,
          special      = 0;
          upperCase    = new RegExp("[A-Z]"),
          numbers      = new RegExp("[0-9]"),
          specialchars = new RegExp("([!,%,&,@,#,$,^,*,?,_,~])");

      if(value.length >= this.options.minChars){
        chars = 1;
      }else{
        chars = -1;
      }
      if(value.match(upperCase)){
        capitals = 1;
      }else{
        capitals = 0;
      }
      if(value.match(numbers)){
        numbers = 1;
      }else{
        numbers = 0;
      }
      if(value.match(specialchars)){
        special = 1;
      }else{
        special = 0;
      }

      secureTotal = chars + capitals + numbers + special;
      securePercentage = (secureTotal / 4) * 100;

      this.addStatus(securePercentage);

    },

    addStatus: function(percentage){
      var status = "",
          text = "Min " + this.options.minChars + " " + this.options.textChar,
          meter = $(this.element).closest(".passtrengthMeter"),
          tooltip = meter.find(".tooltip");

      if(percentage >= 25){
        status = "weak";
        text = this.options.textWeak;
      }
      if(percentage >= 50){
        status = "medium";
        text = this.options.textMedium;
      }
      if(percentage >= 75){
        status = "strong";
        text = this.options.textStrong;
      }
      if(percentage >= 100){
        status = "very-strong";
        text = this.options.textVeryStrong;
      }
	  meter.attr("class", "passtrengthMeter");
	  $(this.element).attr("percentage",percentage);
      if(this.options.bar) meter.addClass(status);
      if(this.options.tooltip){
        tooltip.text(text);
      }
    },

  };

  $.fn[pluginName] = function(options) {
      return this.each(function() {
          if (!$.data(this, "plugin_" + pluginName)) {
              $.data(this, "plugin_" + pluginName, new Plugin(this, options));
          }
      });
  };

})(jQuery, window, document);
